package dependancy;

import dependancy.annotation.InjectAnnot;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class DependancyInjector {

    private IModule module;
    DependancyInjector(IModule module ) {
        this.module=module;
    }

    public Object inject( Class<?> classToInject) throws Exception {
        if (classToInject == null) {
            return null;
        }
        return injectFieldsIntoClass(classToInject);
    }

    private Object injectFieldsIntoClass( Class<?> classToInject) throws Exception {

        for ( Constructor<?> constructor : classToInject.getConstructors()) {
            if (constructor.isAnnotationPresent(InjectAnnot.class)) {
                return injectFieldsViaConstructor(classToInject, constructor);
            } else {
                return injectFields(classToInject);
            }
        }
        return null;
    }

    private Object injectFields(Class<?> classToInject) throws Exception {
        Object o = classToInject.newInstance();
        for (Field field : classToInject.getDeclaredFields()) {
            if(field.isAnnotationPresent(InjectAnnot.class)) {
                final Class<?> dependency = module.getMapping(field.getType());
                field.setAccessible(true);
                field.set(o, dependency.getConstructor().newInstance());
            }
        }
        return o;
    }

    private Object injectFieldsViaConstructor(Class<?> classToInject, Constructor<?> constructor) throws Exception {
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        Object[] objArr = new Object[parameterTypes.length];
        int i = 0;
        for (Class<?> c : parameterTypes) {
            Class<?> dependency = module.getMapping(c);
            if (c.isAssignableFrom(dependency)) {
                objArr[i++] = dependency.getConstructor().newInstance();
            }
        }
        return classToInject.getConstructor(parameterTypes).newInstance(objArr);
    }


}
