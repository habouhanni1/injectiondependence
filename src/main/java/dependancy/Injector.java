package dependancy;

import Example.A;
import Example.B;

import java.lang.reflect.*;

public class Injector <T>{

    public Injector() {

    }

    public void DiscoverDep(Class<T> obj, Object valeur) {

        try {

            Class cls = obj.getClass();


            Field[] fields = cls.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                System.out.println( fields[i].getType().toString());
                if (fields[i].getType() == valeur.getClass()) {
                    System.out.println("Field  to inject : " + fields[i].getType());
                   // new DependancyInjector(obj, valeur);
                }
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public static void main(String[] args) {

        A a =new A();
        B b=new B();
        Injector <A>injector=new Injector();
        //injector.DiscoverDep(a.getClass(),b);
    }
}
